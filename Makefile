#!/usr/bin/make -f

up:
	echo "Running docker-compose up -d"
	docker-compose up -d

down:
	echo "Running docker-compose down"
	docker-compose down

restart:
	echo "Running docker-compose down"
	docker-compose down
	echo "Running docker-compose up -d"
	docker-compose up -d

jumpin:
	echo "Running docker-compose exec app bash"
	docker-compose exec app bash

install:
	echo "Running cp .env.example .env"
	cp .env.example .env

	echo "Running docker-compose up -d"
	docker-compose up -d

	echo "	docker-compose exec app composer install"
	docker-compose exec app composer install


	echo "Running docker-compose exec app php artisan migrate --seed"
	docker-compose exec app php artisan migrate --seed

	echo "Running docker-compose exec app php artisan key:generate"
	docker-compose exec app php artisan key:generate

	echo "Running docker-compose up -d"
	docker-compose up -d

	echo "Application started at http://localhost"

update:
	echo "Running docker-compose exec app composer install"
	docker-compose exec app composer install

	echo "Running docker-compose exec app php artisan migrate --seed"
	docker-compose exec app php artisan migrate --seed

	echo "Running docker-compose exec app php artisan config:clear"
	docker-compose exec app php artisan config:clear

