## Prerequisites
- Docker
- Docker-compose

## Run 
`make install`
- To install the application

`make update`
- To update the application

`make up`
- To start the application

`make down`
- To stop the application

## Installation without Makefile

##### Run all containers
`docker-compose up -d`

##### Install composer dependencies
`docker-compose exec app composer install`

##### Run migrations
`docker-compose exec app php artisan migrate`

##### Run seeds
` docker-compose exec app php artisan migrate --seed`

##### Generate app key
`docker-compose exec app php artisan key:generate`

##### Check out the app at 127.0.0.1 / localhost 

