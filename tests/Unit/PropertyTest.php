<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PropertyTest extends TestCase
{
    use RefreshDatabase;


    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $response = $this->json('POST', '/api/property', [
            'suburb' => 'PARRAMATTA',
            'state' => 'NSW',
            'country' => 'Australia',
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Property created.',
            ]);
    }
}
